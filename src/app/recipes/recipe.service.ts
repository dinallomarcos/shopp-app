import { Injectable, OnInit, EventEmitter } from '@angular/core';
import { Recipe } from './recipe.model';
import { Ingredient } from '../shared/ingredient.model';
import { ShoppingListService } from '../shopping-list/shopping-list.service';

@Injectable({
  providedIn: 'root'
})
export class RecipeService implements OnInit {

  private recipeSelect = new EventEmitter<Recipe>();

  private recipes: Recipe[] = [
    new Recipe('Barbecue',
      'Pork Rib Barbecue',
      'https://cdn.pixabay.com/photo/2016/06/15/19/09/food-1459693_1280.jpg',
      [new Ingredient('Pork Rib', 1), new Ingredient('Salt', 1), new Ingredient('Black Pepper', 1)]),
    new Recipe('Pasta',
      'Pasta ao Pesto',
      'https://c.pxhere.com/photos/99/04/tagliatelle_pasta_on_a_fork_food_italian_meal_cuisine_italy_traditional-1375412.jpg!d',
      [new Ingredient('Pasta', 500), new Ingredient('Wather', 1), new Ingredient('Salt', 1), new Ingredient('Basil', 10)])
  ];

  constructor(private slService: ShoppingListService) { }

  ngOnInit() { }

  public getRecipes(): Recipe[] {
    return this.recipes.slice();
  }

  public getRecipeSelected(): EventEmitter<Recipe> {
    return this.recipeSelect;
  }

  public addIngredientsToShoppingList(ingredients: Ingredient[]) {
    this.slService.addIngredients(ingredients);
  }
}
