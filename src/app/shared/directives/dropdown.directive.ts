import { Directive, HostBinding, HostListener, ElementRef } from '@angular/core';

@Directive({
  selector: '[appDropdown]',
  exportAs: 'child'
})
export class DropdownDirective {

  @HostBinding('class.show') isShow = false;

  constructor() { }

  @HostListener('click') toggleShow() {
    this.isShow = !this.isShow;
  }

}
