export class Ingredient {

  /**
   * Creates an instance of ingredient.
   * Shortcut connstructor adding fields.
   * @param name
   * @param amount
   */
  constructor(public name: string, public amount: number) {}
}
